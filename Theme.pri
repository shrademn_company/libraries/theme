INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

QT += svg

SOURCES += \
	$$PWD/ThemeManager.cpp

HEADERS += $$PWD/resources.hpp \
    $$PWD/ThemeManager.hpp

RESOURCES += \
    $$PWD/Theme.qrc
