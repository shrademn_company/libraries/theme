//------------------------------------------------------------------------------
//
// ThemeManager.cpp created by Yyhrs 2017/01/10
//
//------------------------------------------------------------------------------

#include <QApplication>
#include <QColorDialog>
#include <QFile>
#include <QStyleFactory>

#include "ThemeManager.hpp"

ThemeManager::ThemeManager(const QString &theme)
{
	setTheme(theme);
	QIcon::setThemeSearchPaths(QIcon::themeSearchPaths() << ":/");
	qApp->setStyle(QStyleFactory::create("fusion"));
}

ThemeManager::ThemeManager(const QString &theme, const QString &picture): ThemeManager{theme}
{
	startSplashScreen(picture);
}

void ThemeManager::startSplashScreen(const QString &picture)
{
	m_splash.setPixmap(QPixmap{picture}.scaled(256, 128, Qt::KeepAspectRatio));
	m_splash.show();
	QApplication::processEvents();
}

void ThemeManager::stopSplashScreen(QMainWindow *window)
{
	if (window)
		m_splash.finish(window);
	else
		m_splash.close();
}

QString ThemeManager::getStyleSheet()
{
	QFile styleFile{Stylesheets::c_shrademnCompany};

	if (styleFile.open(QFile::ReadOnly))
	{
		QString styleSheet{QString::fromLatin1(styleFile.readAll())};

		styleSheet.replace("@primary", (QIcon::themeName() == Theme::c_light ? Theme::c_light : Theme::c_dark));
		styleSheet.replace("@secondary", (QIcon::themeName() == Theme::c_dark ? Theme::c_dark : Theme::c_light));
		return styleSheet;
	}
	return {};
}

QPalette ThemeManager::getPalette()
{
	QPalette palette{qApp->palette()};

	if (QIcon::themeName() == Theme::c_light)
	{
		palette.setColor(QPalette::Window, s_colorLightBright);
		palette.setColor(QPalette::WindowText, s_colorDark);    //	0	A general foreground color.
		palette.setColor(QPalette::Base, s_colorLightBright);    //	9	Used mostly as the background color for text entry widgets, but can also be used for other painting - such as the background of combobox drop down lists and toolbar handles. It is usually white or another light color.
		palette.setColor(QPalette::Text, s_colorDark);    //	6	The foreground color used with Base. This is usually the same as the WindowText, in which case it must provide good contrast with Window and Base.
		palette.setColor(QPalette::AlternateBase, s_colorLight);    //	16	Used as the alternate background color in views with alternating row colors (see QAbstractItemView::setAlternatingRowColors()).
		palette.setColor(QPalette::PlaceholderText, Qt::gray);    //	20	Used as the placeholder color for various text input widgets. This enum value has been introduced in Qt 5.12
		palette.setColor(QPalette::Button, s_colorLightBright);    //	1	The general button background color. This background can be different from Window as some styles require a different background color for buttons.
		palette.setColor(QPalette::ButtonText, s_colorDarkDim);    //	8	A foreground color used with the Button color.
		palette.setColor(QPalette::BrightText, Qt::black);    //	7	A text color that is very different from WindowText, and contrasts well with e.g. Dark. Typically used for text that needs to be drawn where Text or WindowText would give poor contrast, such as on pressed push buttons. Note that text colors can be used for things other than just words; text colors are usually used for text, but it's quite common to use the text color roles for lines, icons, etc.
		palette.setColor(QPalette::Light, s_colorLightBright);    //	2	Lighter than Button color.
		palette.setColor(QPalette::Midlight, s_colorLight);    //	3	Between Button and Light.
		palette.setColor(QPalette::Dark, s_colorLightDim);    //	4	Darker than Button.
	}
	else
	{
		palette.setColor(QPalette::Window, s_colorDarkDim);
		palette.setColor(QPalette::WindowText, s_colorLight);    //	0	A general foreground color.
		palette.setColor(QPalette::Base, s_colorDarkDim);    //	9	Used mostly as the background color for text entry widgets, but can also be used for other painting - such as the background of combobox drop down lists and toolbar handles. It is usually white or another light color.
		palette.setColor(QPalette::Text, s_colorLight);    //	6	The foreground color used with Base. This is usually the same as the WindowText, in which case it must provide good contrast with Window and Base.
		palette.setColor(QPalette::AlternateBase, s_colorDark);    //	16	Used as the alternate background color in views with alternating row colors (see QAbstractItemView::setAlternatingRowColors()).
		palette.setColor(QPalette::PlaceholderText, Qt::gray);    //	20	Used as the placeholder color for various text input widgets. This enum value has been introduced in Qt 5.12
		palette.setColor(QPalette::Button, s_colorDarkDim);    //	1	The general button background color. This background can be different from Window as some styles require a different background color for buttons.
		palette.setColor(QPalette::ButtonText, s_colorLightBright);    //	8	A foreground color used with the Button color.
		palette.setColor(QPalette::BrightText, Qt::white);    //	7	A text color that is very different from WindowText, and contrasts well with e.g. Dark. Typically used for text that needs to be drawn where Text or WindowText would give poor contrast, such as on pressed push buttons. Note that text colors can be used for things other than just words; text colors are usually used for text, but it's quite common to use the text color roles for lines, icons, etc.
		palette.setColor(QPalette::Light, s_colorDarkDim);    //	2	Lighter than Button color.
		palette.setColor(QPalette::Midlight, s_colorDark);    //	3	Between Button and Light.
		palette.setColor(QPalette::Dark, s_colorDarkBright);    //	4	Darker than Button.
	}
	palette.setColor(QPalette::Highlight, s_colorBlue);
	palette.setColor(QPalette::HighlightedText, Qt::black);    //	4	Darker than Button.
	palette.setColor(QPalette::ToolTipBase, s_colorYellow);    //	18	Used as the background color for QToolTip and QWhatsThis. Tool tips use the Inactive color group of QPalette, because tool tips are not active windows.
	palette.setColor(QPalette::ToolTipText, Qt::black);    //	19	Used as the foreground color for QToolTip and QWhatsThis. Tool tips use the Inactive color group of QPalette, because tool tips are not active windows.
	palette.setColor(QPalette::Link, s_colorYellow); //	14	A text color used for unvisited hyperlinks. By default, the link color is Qt::blue.
	palette.setColor(QPalette::LinkVisited, s_colorGreen); //	15	A text color used for already visited hyperlinks. By default, the linkvisited color is Qt::magenta.
	return palette;
}

void ThemeManager::cycleTheme()
{
	setTheme(QIcon::themeName() == Theme::c_light ? Theme::c_dark : Theme::c_light);
}

void ThemeManager::setTheme(const QString &theme)
{
	QIcon::setThemeName(theme);
	QApplication::setPalette(getPalette());
	qApp->setStyleSheet(getStyleSheet());
}
