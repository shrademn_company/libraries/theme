//------------------------------------------------------------------------------
//
// ThemeManager.hpp created by Yyhrs 2017/01/10
//
//------------------------------------------------------------------------------

#ifndef THEMEMANAGER_HPP
#define THEMEMANAGER_HPP

#include <QAction>
#include <QMainWindow>
#include <QMap>
#include <QSplashScreen>

#include "resources.hpp"

class ThemeManager: public QObject
{
	Q_OBJECT

public:
	enum SettingsKey
	{
		Theme,
		Color
	};
	Q_ENUM(SettingsKey)

	explicit ThemeManager(const QString &theme = Theme::c_dark);
	explicit ThemeManager(const QString &theme, const QString &picture);
	~ThemeManager() = default;

	void startSplashScreen(const QString &picture = Logos::c_shrademnCompanyRed.arg(Theme::c_light));
	void stopSplashScreen(QMainWindow *window = nullptr);

	static QString  getStyleSheet();
	static QPalette getPalette();

	static void setTheme(const QString &theme);
	static void cycleTheme();

	static inline const QString s_colorBlue{QStringLiteral("#FF3DAEE9")};
	static inline const QString s_colorGreen{QStringLiteral("#FF27AE60")};
	static inline const QString s_colorRed{QStringLiteral("#FFDA4453")};
	static inline const QString s_colorYellow{QStringLiteral("#FFEDC11E")};
	static inline const QString s_colorDarkBright{QStringLiteral("#FF555555")};
	static inline const QString s_colorDark{QStringLiteral("#FF4D4D4D")};
	static inline const QString s_colorDarkDim{QStringLiteral("#FF373737")};
	static inline const QString s_colorLightBright{QStringLiteral("#FFF7F8F9")};
	static inline const QString s_colorLight{QStringLiteral("#FFEFF0F1")};
	static inline const QString s_colorLightDim{QStringLiteral("#FFE7E8E9")};

private:
	void loadDefaultValues();

	QSplashScreen m_splash;
};
#endif // THEMEMANAGER_HPP
